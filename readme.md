# Ciframos

Try to automate Let's Ecnrypt certificate for GitLab Pages. As much as possible.

## See also

  * [Let's Encrypt](https://letsencrypt.org/)
  * [Ruby client](https://github.com/unixcharles/acme-client)
  * [Node.js client](https://github.com/themadcreator/acme-express)
